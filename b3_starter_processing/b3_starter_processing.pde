/* Import libraries: */
import processing.serial.*;   // Imports serial library

/* Global variables: */
Serial serial_port;           // Controls the serial port (do not modify)
final int port_num = 32;       // Port number (change to yours!!) 

long cval0 = -1;              // Sensor value 0.
long cval1 = -1;              // Sensor value 1.
long cval2 = -1;              // Sensor value 2.
long cval3 = -1;              // Sensor value 3.
long cval4 = -1;              // Sensor value 4.
long cval5 = -1;              // Sensor value 5.
long cval6 = -1;              // Sensor value 6.
long cval7 = -1;              // Sensor value 7.
long cval8 = -1;              // Sensor value 8.
long cval9 = -1;              // Sensor value 9.
long cval10 = -1;             // Sensor value 10.


/* Program entry-point */
void setup() {
  // Set up canvas:
  // ... Your code goes here ...
  
  
  // ==== Do not modify ==========================
  // Show available ports for Serial communication:
  for(int i = 0; i < Serial.list().length; i++) {
    println("Port " + str(i) + ": " + Serial.list()[i]);
  }
  
  // Connect to one port:
  String portName = Serial.list()[port_num]; 
  serial_port = new Serial(this, portName, 9600);
  // =============================================
}

/* Main loop: */
void draw() {
  background(0);
  readValues(serial_port);  // Reads input from serial port and saves data to cvalX.

  
  /* ... Your code goes here ... 
    Use variables cvalX to generate your 
    own effect / animation / graphics
   */
  
}


/* Function to read from the Arduino: */
void readValues(Serial p) {
  String msg = p.readStringUntil(10);
  
  if(msg != null) { 
    //println(msg); // uncomment this to test the sensor woth low signal levels.
    String[] msg_split = split(msg, "_");
    if(msg_split.length >= 2) {
      
      try {
        switch(msg_split[0]) {
          case "S0":
            cval0 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S1":
            cval1 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S2":
            cval2 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S3":
            cval3 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S4":
            cval4 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S5":
            cval5 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S6":
            cval6 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S7":
            cval7 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S8":
            cval8 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S9":
            cval9 = Long.parseLong(trim(msg_split[1]));
            break;
          case "S10":
            cval10 = Long.parseLong(trim(msg_split[1]));
            break;
          default:
            println("No valid data: \'" + msg + "\'");
            break;
        }
      } catch(Exception e) {
        println("No valid data (E): \'" + msg + "\'");
      }
    } else {
      println("Wrong format");
    }
  }
}
  
