/** Import libraries :*/
#include <CapacitiveSensor.h>

/** Define constants: */
#define SENSE_PIN_0 7
#define SENSE_PIN_1 8
#define SIGNAL_PIN  4

/** Global variables: */
CapacitiveSensor s0 = CapacitiveSensor(SIGNAL_PIN, SENSE_PIN_0);
CapacitiveSensor s1 = CapacitiveSensor(SIGNAL_PIN, SENSE_PIN_1);

/* Program entry point: */
void setup() {
  Serial.begin(9600);
}

/* Main loop: */
void loop() {
  long cval0 = s0.capacitiveSensor(30);
  long cval1 = s1.capacitiveSensor(30);
  
  /* ... add your post-processing lines here to adjust/correct the read values ... */

  // Send each value to processing.

  /** NOTE: 
      In order to send values to processing, you must preceed each number with "SX_", 
      where 'X' is the sensor number. Send as many as you have.
   */
  Serial.println("S0_" + String(cval0));
  Serial.println("S1_" + String(cval1));
  
  delay(10);
}
