# CodiCreatiu

Scripts to connect a capacitive sensor (reading the input with arduino) with processing.

## Arduino script 
- the b3_starter_arduino.ino uses the CapacitiveSensor library, you can read about it here: https://www.arduino.cc/reference/en/libraries/capacitivesensor/
- you have to intall the library
- upload the script b3_starter_arduino.ino into your arduino board (selecting the right board and port)
- unplugged the arduino to liberate the port

## Processing script
- Select the port number port_num = ? Note that when you run the script you will have all the ports displayed. Select the one that has the arduino board
- It is possible to use the Arduino IDE to find the port in case the processing print is not super clear


